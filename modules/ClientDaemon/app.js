"use strict";

const dotenv = require("dotenv").config();
const chalk = require("chalk");
const axios = require("axios");
const http = require("http");
const https = require("https");
const { spawn } = require("child_process");

var fs = require("fs");

// Using the Node.js Device SDK for IoT Hub:
//   https://github.com/Azure/azure-iot-sdk-node
// The sample connects to a device-specific MQTT endpoint on your IoT Hub.
var Mqtt = require("azure-iot-device-mqtt").Mqtt;
var DeviceClient = require("azure-iot-device").Client;
var Message = require("azure-iot-device").Message;

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

// The device connection string to authenticate the device with your IoT hub.
//
// NOTE:
// For simplicity, this sample sets the connection string in code.
// In a production environment, the recommended approach is to use
// an environment variable to make it available to your application
// or use an HSM or an x509 certificate.
// https://docs.microsoft.com/azure/iot-hub/iot-hub-devguide-security
//
// Using the Azure CLI:
// az iot hub device-identity show-connection-string --hub-name {YourIoTHubName} --device-id MyNodeDevice --output table
//var connectionString = process.env.CONNECTION_STRING;
//console.log(process.env);

//var connectionString =
//  "HostName=enact-hub.azure-devices.net;DeviceId=GenesisRaspberry2;SharedAccessKey=I8OYIYD/ayPB8RNOwlHZ+42CpgpgCJS6ET+HS1C0dJI=";

// var connectionString; //= open("/opt/gateway/connection.credential", "r").readline()
// fs.readFile('connection.credential', 'utf8', function (err, data) {  
//   if (err) {       
//     return console.error(err);
//   }
//   console.log(data);
//   connectionString = data;
// });


// if (process.argv.length > 2) {
//   console.log("Received command line argument: " + process.argv[2]);
//   deploy(process.argv[2]);
// } else {
//   console.log("No command line argument received!");
// }

//var myArgs = process.argv.slice(2);

async function deploy(model_url) {
  // var genesisModel = '';

  // https.get(model_url, (res) => {
  //   const { statusCode } = res;
  //   const contentType = res.headers['content-type'];

  //   let error;
  //   if (statusCode !== 200) {
  //     error = new Error('Request Failed.\n' +
  //                       `Status Code: ${statusCode}`);
  //   }
  //   //else if (!/^application\/json/.test(contentType)) {
  //   //  error = new Error('Invalid content-type.\n' +
  //   //                    `Expected application/json but received ${contentType}`);
  //   //}
  //   if (error) {
  //     console.error(error.message);
  //     // Consume response data to free up memory
  //     res.resume();
  //     return;
  //   }

  //   res.setEncoding('utf8');
  //   let rawData = '';
  //   res.on('data', (chunk) => { rawData += chunk; });
  //   res.on('end', () => {
  //     try {
  //       genesisModel = JSON.parse(rawData);
  //       console.log(genesisModel);
  //     } catch (e) {
  //       console.error(e.message);
  //     }
  //   });
  // }).on('error', (e) => {
  //   console.error(`Got error: ${e.message}`);
  // );
  try {
    console.log(chalk.green(model_url));
    let res = await axios.get(model_url);
    let genesisModel = res.data;
    console.log(chalk.green("Received model: "));
    console.log(chalk.green(JSON.stringify(genesisModel)));

    while (true) {
      try {
        let genesis_res = await axios.post(
          "http://127.0.0.1:8880/genesis/deploy",
          genesisModel,
          { headers: { "Content-Type": "application/json" } }
        );
        if ("started" in genesis_res.data) {
          console.log(chalk.green(JSON.stringify(genesis_res.data)));
          console.log(chalk.green("Deployment started by GeneSIS"));
          break;
        }
        console.log(chalk.green("Waiting for GeneSIS..."));
        await sleep(2000);
      } catch (error) {
        console.log(chalk.green("Wait a bit for GeneSIS to start..."));
        await sleep(2000);
      }
    }
  } catch (error) {
    console.log(chalk.red("Some error happened: \n" + error.toString()));
  }
}

/**
 * Function to handle the TriggerDeployment direct method call from IoT hub
 *
 * @param {*} request
 * @param {*} response
 */
function onTriggerDeployment(request, response) {
  //TODO: check with GeneSIS API what method to call
  // Function to send a direct method reponse to your IoT hub.
  function directMethodResponse(err) {
    if (err) {
      console.error(
        chalk.red(
          "An error ocurred when sending a method response:\n" + err.toString()
        )
      );
    } else {
      console.log(
        chalk.green(
          "Response to method '" + request.methodName + "' sent successfully."
        )
      );
    }
  }

  console.log(chalk.green("Direct method payload received:"));
  console.log(chalk.green(request.payload));

  //Pass the received model for deployment
  deploy(request.payload);
}

/**
 * A mockup function to receive direct method invocations.
 *
 * @param {*} request
 * @param {*} response
 */
async function onDirectMethodInvocation(request, response) {
  // Function to send a direct method reponse to your IoT hub.
  function directMethodResponse(err) {
    if (err) {
      console.error(
        chalk.red(
          "An error ocurred when sending a method response:\n" + err.toString()
        )
      );
    } else {
      console.log(
        chalk.green(
          "Response to method '" + request.methodName + "' sent successfully."
        )
      );
    }
  }
  console.log(chalk.green("Direct method payload received:"));
  console.log(chalk.green(request.payload));

  //TODO: to do something with the message here...

  // Report success back to your hub.
  //response.send(200, "Remote method invoked", directMethodResponse);
  console.log("now comes arduino")
  let arduino = await checkArduino()
  console.log(arduino)
  response.send(200, {"arduino": arduino}, directMethodResponse)
}

/**
 * A mockup function to receive C2D messages.
 *
 * @param {*} msg
 */
function onC2DMessage(msg) {
  console.log(chalk.green("Id: " + msg.messageId + " Body: " + msg.data));
  client.complete(msg, function (err) {
    if (err) {
      console.error(chalk.red("Message complete error: " + err.toString()));
    } else {
      //TODO: to do something with the message
      deploy(msg.data.toString());
      console.log(chalk.green("Message complete sent"));
    }
  });
}

function checkArduino() {
  return new Promise(resolve =>{
    const lsusb = spawn("ls", ["/dev/"])
    lsusb.stdout.on("data", data => {
      let output = `${data}`
      console.log(output)
      if(output.includes('ttyACM0')){  //TODO: find a magic code for Arduino
        resolve(true)
      }
      else{
        resolve(false)
      }
    })
    lsusb.stderr.on("data", data=>{
      console.log(`${data}`)
    })
  })
  
}


/**
 * A mockup function to receive direct method invocations.
 *
 * @param {*} request
 * @param {*} response
 */
async function onRuntimeStatus(request, response) {
  // Function to send a direct method reponse to your IoT hub.
  function directMethodResponse(err) {
    if (err) {
      console.error(
        chalk.red(
          "An error ocurred when sending a method response:\n" + err.toString()
        )
      );
    } else {
      console.log(
        chalk.green(
          "Response to method '" + request.methodName + "' sent successfully."
        )
      );
    }
  }
  console.log(chalk.green("Direct method payload received:"));
  console.log(chalk.green(request.payload));

  console.log("checking if Arduino is attached")
  let arduino = await checkArduino()
  console.log(arduino)
  response.send(200, {"arduino": arduino}, directMethodResponse)
}


//Set up the handlers for the direct method call and C2D messages.
var client = DeviceClient.fromConnectionString(
  fs.readFileSync('/opt/gateway/connection.credential', 'UTF-8'), 
  //connectionString,
  Mqtt
);
client.onDeviceMethod("invokeDirectMethod", onDirectMethodInvocation);
client.onDeviceMethod("invokeRuntimeStatus", onRuntimeStatus);
client.on("message", onC2DMessage);

// (async ()=>{
//   let res = await checkArduino()
//   console.log(res)
// })()
