"use strict";

require('log-timestamp');
var chalk = require("chalk");
var fs = require("fs");
var chokidar = require('chokidar');
const axios = require('axios');


// Using the Node.js Device SDK for IoT Hub:
//   https://github.com/Azure/azure-iot-sdk-node
var amqp = require("azure-iot-device-amqp").Amqp;
var DeviceClient = require("azure-iot-device").Client;

// The device connection string to authenticate the device with your IoT hub.
//
// NOTE:
// For simplicity, this sample sets the connection string in code.
// In a production environment, the recommended approach is to use
// an environment variable to make it available to your application
// or use an HSM or an x509 certificate.
// https://docs.microsoft.com/azure/iot-hub/iot-hub-devguide-security
//
// Using the Azure CLI:
// az iot hub device-identity show-connection-string --hub-name {YourIoTHubName} --device-id MyNodeDevice --output table
//var connectionString = process.env.CONNECTION_STRING;
//console.log(process.env);
var client = DeviceClient.fromConnectionString(  
  fs.readFileSync("/opt/gateway/connection.credential", "UTF-8"),
  //connectionString,
  amqp
);

var watcher = chokidar.watch(['/dev/ttyACM*','/dev/ttyUSB*'], {persistent: true, usePolling: true, depth: 0});
console.log(chalk.green("Connected Arduino Detector is up an running."));

/**
 * Get the approximate location of the device based on IP. 
 */
let getLoc = async () => {
    let resp = await axios.get('http://extreme-ip-lookup.com/json/');
    let unwrap = ({city, lat, lon}) => ({city, lat, lon});
    let result = unwrap(resp.data);
    updateProperty(result); 
    setTimeout(getLoc, 5 * 60 * 1000); //Update the location every 5 minutes
}

let getDockerContainers = async () => {
  let resp = await axios.get('http://172.17.0.1:2376/containers/json');
  let images = resp.data
    .map((item) => {return item.Image})
    .filter((item) => {return !item.startsWith("sha256:") });
  let result = {containerImages: images};
  updateProperty(result);
  setTimeout(getDockerContainers, 1 * 60 * 1000);
}

getLoc();
getDockerContainers();


watcher
  .on('add', function(path) {
    console.log(chalk.green('File', path, 'has been added'));
    console.log(watcher.getWatched()['/dev'].length);
    if (watcher.getWatched()['/dev'].length > 0) {
      var patch = {
        arduino: "on"
      };
      updateProperty(patch);
    }
  })  
  .on('unlink', function(path) {
    console.log(chalk.green('File', path, 'has been removed'));
    console.log(watcher.getWatched()['/dev'].length);
    if (watcher.getWatched()['/dev'].length == 0) {
      var patch = {
        arduino: "off"
      };
      updateProperty(patch);
    }
  })
  .on('error', function(error) {console.error('Error happened', error);})

// Send a reported property update patch
function updateProperty(patch) {
  client.open(function (err) {
    if (err) {
      console.error(
        chalk.red("Could not open IoT Hub device client", err.toString())
      );
      //process.exit(-1);
    } else {
      // Get device Twin
      client.getTwin(function (err, twin) {
        if (err) {
          console.error(chalk.red("Could not get device twin", err.toString()));
        } else {
          console.log(chalk.green("Device twin acquired"));
          console.log(chalk.green("Applying patch: " + JSON.stringify(patch)));
          twin.properties.reported.update(patch, function (err) {
            if (err) {
              console.error(chalk.red("Something went wrong", err.toString()));
            } else {
              console.log(
                chalk.green(
                  "Device state reported with: " + JSON.stringify(patch)
                )
              );
            }
          });
        }
      });
    }
  });
}
