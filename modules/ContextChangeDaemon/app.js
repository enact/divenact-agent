"use strict";

require('log-timestamp');
var chalk = require("chalk");
var SerialPort = require("serialport");
var usbDetect = require("usb-detection");
var fs = require("fs");

// Using the Node.js Device SDK for IoT Hub:
//   https://github.com/Azure/azure-iot-sdk-node
var Mqtt = require("azure-iot-device-mqtt").Mqtt;
var DeviceClient = require("azure-iot-device").Client;

// The device connection string to authenticate the device with your IoT hub.
//
// NOTE:
// For simplicity, this sample sets the connection string in code.
// In a production environment, the recommended approach is to use
// an environment variable to make it available to your application
// or use an HSM or an x509 certificate.
// https://docs.microsoft.com/azure/iot-hub/iot-hub-devguide-security
//
// Using the Azure CLI:
// az iot hub device-identity show-connection-string --hub-name {YourIoTHubName} --device-id MyNodeDevice --output table
//var connectionString = process.env.CONNECTION_STRING;
//console.log(process.env);
var client = DeviceClient.fromConnectionString(
  fs.readFileSync("/opt/gateway/connection.credential", "UTF-8"),
  //deviceConnectionString,
  Mqtt
);

async function getSerialPort() {
  SerialPort.list().then((ports) => {
    var status = "off";
    ports.forEach(function (port) {
      console.log(chalk.green(JSON.stringify(port)));
      if (checkArduino(port)) {
        console.log(chalk.green("Arduino is available on: " + port.path));
        status = "on";
      }
    });

    //reported Arduino status
    var patch = {
      arduino: status,
    };

    updateProperty(patch);
  });
}

function checkArduino(port) {
  if (
    (port.hasOwnProperty("manufacturer") &&
      port.manufacturer !== undefined &&
      port.manufacturer.toLowerCase().includes("arduino")) ||
    (port.hasOwnProperty("pnpId") &&
      port.pnpId !== undefined &&
      port.pnpId.toLowerCase().includes("arduino"))
  ) {
    return true;
  }
  return false;
}

//Listen for events
usbDetect.startMonitoring();
console.log(chalk.green("Connected Arduino Daemon is up an running."));

//device connected
usbDetect.on("add", function (device) {
  console.log(chalk.green("New USB device plugged in!"));
  console.log(chalk.green(JSON.stringify(device)));
  getSerialPort();
});

//device disconnected
usbDetect.on("remove", function (device) {
  console.log(chalk.green("USB device unplugged!"));
  console.log(chalk.green(JSON.stringify(device)));
  getSerialPort();
});

// Send a reported property update patch
async function updateProperty(patch) {
  client.open(function (err) {
    if (err) {
      console.error(
        chalk.red("Could not open IoT Hub device client", err.toString())
      );
      //process.exit(-1);
    } else {
      // Get device Twin
      client.getTwin(function (err, twin) {
        if (err) {
          console.error(chalk.red("Could not get device twin", err.toString()));
        } else {
          console.log(chalk.green("Device twin acquired"));
          twin.properties.reported.update(patch, function (err) {
            if (err) {
              console.error(chalk.red("Something went wrong", err.toString()));
            } else {
              console.log(
                chalk.green(
                  "Device state reported with: " + JSON.stringify(patch)
                )
              );
            }
          });
        }
      });
    }
  });
}
