"use strict";

require("log-timestamp");
const fetch = require("node-fetch");
const chalk = require("chalk");
const waitPort = require("wait-port");
const axios = require("axios");

var fs = require("fs");
var Client = require("azure-iot-device").ModuleClient;
var Protocol = require("azure-iot-device-amqp").Amqp;

const params = {
  host: "172.18.0.1",
  port: 8000,
};

// Crete device client from connection string.
var connectionString = fs.readFileSync("/opt/gateway/connection.credential")
//  "HostName=enact-div-runtime.azure-devices.net;DeviceId=EmptyRaspberry;SharedAccessKey=Kyxf6ZiTQ2PvU56J7YDUFMi720zRPKfbH7or1FGR/u0=";
//"HostName=enact-div-runtime.azure-devices.net;DeviceId=EmptyRaspberry;ModuleId=GenesisBroker;SharedAccessKey=e63YDCsIrC6Q6ORxqpgssEmUJbO1aDIcIkv0YRGuA/U=";
connectionString = connectionString + ";ModuleId=GenesisBroker";
var moduleClient = Client.fromConnectionString(connectionString, Protocol);

moduleClient.on("error", function (err) {
  console.error(err.message);
});

// connect to the hub
moduleClient.open(function (err) {
  if (err) {
    console.error(chalk.red("Error connecting to hub: " + err));
    process.exit(1);
  }
  console.log(chalk.green("GenesisBroker module client opened"));
  // Create device Twin
  moduleClient.getTwin(function (err, twin) {
    if (err) {
      console.error(chalk.red("Error getting module twin: " + err));
      process.exit(1);
    }
    // Output the current properties
    console.log(chalk.green("Module twin contents:"));
    console.log(chalk.green(twin.properties));
    // Add a handler for desired property changes
    twin.on("properties.desired", function (delta) {
      console.log("New desired properties received:");
      console.log(JSON.stringify(delta));
      //TODO: get the already deployed model from Genesis
      var genesis_model = delta.genesis_model;
      console.log(chalk.green(genesis_model));
      callGenesis(genesis_model)
        .then(() => {
          var patch = {
            genesis_model: genesis_model,
          };
          // send the patch
          twin.properties.reported.update(patch, function (err) {
            if (err) throw err;
            console.log(chalk.green("Module twin state reported"));
          });
        })
        .catch((err) => {
          console.error(chalk.red("Error deploying model to Genesis: " + err));
          process.exit(1);
        });
    });
  });
});

async function callGenesis(genesis_model) {
  waitPort(params)
    .then(async (open) => {
      if (open) {
        console.log(
          chalk.green(
            "GeneSIS is available on: " + params.host + ":" + params.port
          )
        );
        var model = await getModel(genesis_model);
        console.log(chalk.green(model));
        await pushModel(model);
      } else
        console.log(chalk.red("GeneSIS did not reply before the timeout..."));
    })
    .catch((err) => {
      console.error(
        chalk.red(
          "An unknown error occured while waiting for GeneSIS",
          err.toString()
        )
      );
    });
}

async function getModel(url) {
  var response = await fetch(url);
  var model = await response.json();
  return model;
}

async function pushModel(model) {
  //console.log(chalk.green(JSON.stringify(model)));

  var headers = {
    "Content-Type": "application/json",
  };

  await axios
    .post("http://172.18.0.1:8000/genesis/push_model", model, headers)
    .then(function (response) {
      console.log(chalk.green(JSON.stringify(response.data)));
      console.log(chalk.green("Now triggering the deployemnt"));
    })
    .catch(function (error) {
      console.log(chalk.red("Could not push a model to GeneSIS."));
      console.log(chalk.red(error.toString()));
    });

  await axios
    .get("http://172.18.0.1:8000/genesis/deploy_model")
    .then(function (response) {
      console.log(chalk.green("Successfull last mile deployment!"));
      console.log(chalk.green(JSON.stringify(response.data)));
    })
    .catch(function (error) {
      console.log(chalk.red("Could not trigger the model by GeneSIS."));
      console.log(chalk.red(error.toString()));
    });
}
