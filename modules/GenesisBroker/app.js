"use strict";

require('log-timestamp');
const chalk = require("chalk");
const axios = require("axios");
const waitPort = require("wait-port");

var fs = require("fs");

// Using the Node.js Device SDK for IoT Hub:
//   https://github.com/Azure/azure-iot-sdk-node
// The sample connects to a device-specific MQTT endpoint on your IoT Hub.
var Mqtt = require("azure-iot-device-mqtt").Mqtt;
var DeviceClient = require("azure-iot-device").Client;
var Message = require("azure-iot-device").Message;

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

// The device connection string to authenticate the device with your IoT hub.
//
// NOTE:
// For simplicity, this sample sets the connection string in code.
// In a production environment, the recommended approach is to use
// an environment variable to make it available to your application
// or use an HSM or an x509 certificate.
// https://docs.microsoft.com/azure/iot-hub/iot-hub-devguide-security
//
// Using the Azure CLI:
// az iot hub device-identity show-connection-string --hub-name {YourIoTHubName} --device-id MyNodeDevice --output table
//var connectionString = process.env.CONNECTION_STRING;
//console.log(process.env);

//var connectionString =
//  "HostName=enact-hub.azure-devices.net;DeviceId=GenesisRaspberry2;SharedAccessKey=I8OYIYD/ayPB8RNOwlHZ+42CpgpgCJS6ET+HS1C0dJI=";

// var connectionString; //= open("/opt/gateway/connection.credential", "r").readline()
// fs.readFile('connection.credential', 'utf8', function (err, data) {
//   if (err) {
//     return console.error(err);
//   }
//   console.log(data);
//   connectionString = data;
// });

if (process.argv.length > 2) {
  var modelPath = process.argv[2];
  console.log(chalk.green("Genesis model path: " + modelPath));

  const params = {
    host: "172.18.0.1",
    port: 8000,
  };

  waitPort(params)
    .then((open) => {
      if (open) {
        console.log(
          chalk.green(
            "GeneSIS is available on: " + params.host + ":" + params.port
          )
        );

        // Crete device client from connection string.
        var client = DeviceClient.fromConnectionString(
          fs.readFileSync("/opt/gateway/connection.credential", "UTF-8"),
          //connectionString,
          Mqtt
        );

        // Open the client and set up the handlers for the direct method call and C2D messages.
        client.open(function (err) {
          if (err) {
            console.error(chalk.red("Could not open IotHub client"));
          } else {
            console.log(chalk.green("Azure IoT Hub client opened."));
            deploy(modelPath);
          }
        });
      } else
        console.log(chalk.red("GeneSIS did not reply before the timeout..."));
    })
    .catch((err) => {
      console.error(
        chalk.red("An unknown error occured while waiting for GeneSIS", err.toString())
      );
    });
} else {
  console.log(chalk.red("No command line argument received!"));
  process.exit(0);
}

async function deploy(model_path) {
  //try {
  console.log(chalk.green(model_path));
  //while (true) {
  try {
    var data = fs.readFileSync(model_path, "utf8");
    //console.log(chalk.green(data));
    var headers = {
      "Content-Type": "application/json",
    };

    let genesis_res = await axios
      .post(
        "http://172.18.0.1:8000/genesis/push_model",
        JSON.parse(data),
        headers
      )
      .then(function (response) {
        console.log(chalk.green(JSON.stringify(response.data)));
        console.log(chalk.green("Now triggering the deployemnt"));

        axios
          .get("http://172.18.0.1:8000/genesis/logs")
          .then(function (response) {
            console.log(chalk.green("Successfull last mile deployment!"));
            //console.log(chalk.green(JSON.stringify(response.data)));
          })
          .catch(function (error) {
            console.log(chalk.red("Could not trigger the model by GeneSIS."));
            console.log(chalk.red(error.toString()));
          });
      })
      .catch(function (error) {
        console.log(chalk.red("Could not push a model to GeneSIS."));
        console.log(chalk.red(error.toString()));
      });
  } catch (error) {
    console.log(chalk.red("Some error happened: \n" + error.toString()));
  } finally {
    await sleep(5000);
    process.exit(0);
  }
}
