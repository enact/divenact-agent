"use strict";

require("log-timestamp");
const axios = require("axios");
const axiosRetry = require("axios-retry");
const chalk = require("chalk");
const SerialPort = require("serialport");

const Readline = require("@serialport/parser-readline");

var usb_port = "/dev/ttyACM0";
if (process.argv.length > 2) {
  usb_port = process.argv[2];
} else {
  console.log(chalk.red("No port address passed via command line. Using default value: " + usb_port));
}

const port = new SerialPort(usb_port, { baudRate: 9600 });
port.pipe(new Readline({ delimiter: "\r\n" })); // Read the port data

var device_code;
var user_code;
var access_token;
var user_dir;

axiosRetry(axios, {
  retries: 99, // number of retries,
  shouldResetTimeout: true,
  retryDelay: (retryCount) => {
    console.log(`retry attempt: ${retryCount}`);
    return 1000; // time interval between retries
  },
  retryCondition: (error) => {
    console.log(error.response.status);
    return error.response.status !== 200;
  },
});

port.on("open", () => {
  console.log("Alarm button bridge - port open");
});

port.on("data", async (data) => {
  console.log("Received message: " + data.toString());
  console.log(JSON.parse(data));

  if (JSON.parse(data).hasOwnProperty("enrollment")) {
    console.log(JSON.parse(data)["enrollment"]);

    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
    };

    var payload =
      "client_id=mortexsa&scope=email+openid+profile+security&myDeviceId=alarm_button&myDeviceSerialNum=1234&device_code_validity_secondes=300";

    await axios
      .post(
        "https://enact-caac-auth.evidian.com/choice/oidc/devicecode",
        payload,
        headers
      )
      .then((res) => {
        console.log(res.data);
        device_code = res.data["device_code"];
        user_code = res.data["user_code"];
        port.write(JSON.stringify({ user_code: user_code }));
      })
      .catch((error) => {
        console.error(error);
      });

    var payload =
      "grant_type=urn:ietf:params:oauth:grant-type:device_code&device_code=" +
      device_code +
      "&client_id=mortexsa&client_secret=mortexsasecret";
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
    };

    await axios
      .post(
        "https://enact-caac-auth.evidian.com/choice/oidc/token",
        payload,
        headers
      )
      .then(async (response) => {
        console.log(JSON.stringify(response.data));
        port.write(JSON.stringify({ status: 200 }));
        access_token = response.data["access_token"];
        console.log(access_token);
        await axios
          .post(
            "https://enact-caac-my.evidian.com/oidc-provider/userinfo",
            {}, //empty data
            {
              headers: {
                Authorization: `Bearer ${access_token}`,
              },
            }
          )
          .then((response) => {
            console.log(JSON.stringify(response.data));
            user_dir = response.data["sub"];
          });
      })
      .catch((err) => {
        if (err.response.status !== 200) {
          throw new Error(
            `Failed accessing CAAC server with status code: ${err.response.status}`
          );
        }
      });
  } else if (JSON.parse(data).hasOwnProperty("alarm")) {
    var alarm = JSON.parse(data)["alarm"];
    console.log(alarm);
    console.log(access_token);

    await axios
      .post(
        "https://enact-caac-context-input.evidian.com/contentListener",
        { userid: user_dir, button: alarm },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${access_token}`,
          },
        }
      )
      .then((response) => {
        console.log(JSON.stringify(response.status));
      });
  }
});